package quotesparser;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

public class CsvQuotesSource {

	public interface TextReader {
		String readLine();
	}

	public static class Quote {
		String symbol;
		private double bid;
		private long time;

		public Quote(String symbol, long time, double bid) {
			this.symbol = symbol;
			this.time = time;
			this.bid = bid;
		}

		public long getTime() {
			return time;
		}

		public String symbol() {
			return symbol;
		}

		public double bid() {
			return bid;
		}
	}

	private TextReader reader;

	public CsvQuotesSource(TextReader reader) {
		this.reader = reader;
	}

	public Quote next() {
		String line = reader.readLine();
		if (line == null)
			return null;
		return parseLine(line);
	}

	private Quote parseLine(String line) {
		String[] tokens = line.split(",");
		String symbol = tokens[0];
		double bid = Double.parseDouble(tokens[3]);
		LocalDateTime localDateTime = LocalDateTime.of(LocalDate.parse(tokens[1]), LocalTime.parse(tokens[2]));
		long time = localDateTime.atOffset(ZoneOffset.UTC).toInstant().toEpochMilli();
		return new Quote(symbol, time, bid);
	}
}
