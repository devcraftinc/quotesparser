package quotesparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.junit.Test;
import org.mockito.Mockito;

import quotesparser.CsvQuotesSource.Quote;
import quotesparser.CsvQuotesSource.TextReader;

public class CsvQuotesParserTest {

	private TextReader textReader = Mockito.mock(TextReader.class);
	private CsvQuotesSource p = new CsvQuotesSource(textReader);

	@Test
	public void shouldReturnNullOnEOS() throws ParseException {
		when(textReader.readLine()).thenReturn(null);
		assertNull(p.next());
	}

	@Test
	public void shouldParseScvLineToQuote() throws ParseException {
		when(textReader.readLine()).thenReturn("EBAY,2000-12-30,10:00:00,50.0,51.0");
		Quote q = p.next();
		assertEquals("EBAY", q.symbol());
		assertEquals(50.0, q.bid(), 0);
		assertEquals(toEpoch(LocalDateTime.of(2000, 12, 30, 10, 0, 0)), q.getTime());
	}

	private long toEpoch(LocalDateTime of) {
		return of.atOffset(ZoneOffset.UTC).toInstant().toEpochMilli();
	}

}